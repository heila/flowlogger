# README #

This project is a dependency for code instrumented with JShadow. It accepts data on method input parameters and return values that get logged to the Android logging system.

## Set up ##

Import this as an Eclipse project. 
Export it as a JAR file and place it inside the target Android applications 'libs' directory.

## Who do I talk to? ##

S.E.A. (Sean) Nel or Heila van der Merwe.

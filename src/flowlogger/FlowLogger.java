package flowlogger;

import java.util.Random;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.mapper.Mapper;

/**
 * This class receives requests from instrumented applications to print logs (from within Android). It formats
 * parameter and return value logs using the serialization library XStream. It could work with JSON or binary
 * instead, but there are limitations.
 * 
 * TODO make sure to adjust if running on Dalvik vs JVM (android.os.Build.VERSION.SDK_INT for example is not
 * available on JVM)
 * 
 * @see http://x-stream.github.io/json-tutorial.html
 * @author S. E. A. Nel, Heila
 */
public class FlowLogger {
  protected static String tag = FlowLogger.class.getName();
  protected static int runNumber = new Random().nextInt();
  protected static int androidVersion = 0;
  // android.os.Build.VERSION.SDK_INT;

  protected static XStream xstream = new XStream();

  static {
    // Replaces the default Converter for these classes
    xstream.registerConverter(new CursorConverter());
    Mapper mapper = xstream.getMapper();
    ReflectionProvider reflectionProvider = xstream.getReflectionProvider();
    xstream.registerConverter(new BundleConverter(mapper, reflectionProvider));
    xstream.registerConverter(new ParcelConverter());
    xstream.registerConverter(new ContextConverter());
  }

  /**
   * Logs the given string.
   * 
   * @param msg
   *          String to log.
   */
  protected static void log(String msg) {
    // Disable this when running from emulator (otherwise both may appear in
    // logs.)
    // System.err.println(tag + ": " + msg);
    // Only enable this when running from emulator.
    android.util.Log.i(tag, msg);
  }

  public static String getParamString(String parameterSignature,

  Object[] parameters) {
    StringBuilder s = new StringBuilder();
    Class<?>[] parameterTypes = getTypes(parameterSignature, parameters.length);
    for (int i = 0; i < parameters.length; i++) {
      s.append(serialized(parameters[i], parameterTypes[i]));
      if (i + 1 < parameters.length) {
        s.append(", ");
      }
    }
    return s.toString();
  }

  /**
   * Logs a method's input parameters and return value.
   * 
   * @param className
   *          The class that the method belongs to.
   * @param methodName
   *          The method's name.
   * @param returnSignature
   *          The method's return signature (that defines its return type).
   * @param parameterSignature
   *          The method's parameter signature.
   * @param returnValue
   *          The method's return value.
   * @param parameters
   *          The parameters passed to the method.
   */
  public static void logMethod(String className, String methodName, String returnSignature,
                               String parameterSignature, Object returnValue, String parameters) {

    StringBuilder s = new StringBuilder();

    Class<?> returnType = getType(returnSignature);

    // Prints the method signature.
    s.append(String.format("%s.%s%s\n", className, methodName, parameterSignature));

    // Prints the parameters in tuple format.
    s.append('(');
    s.append(parameters);
    s.append(") => ");

    // Prints the return value.
    s.append(serialized(returnValue, returnType));

    log(s.toString());
  }

  /**
   * Gets an array of type classes specified in the signature.
   * 
   * @param signature
   *          The parameter signature.
   * @param numparams
   *          The number of parameters.
   * @return An array of types specified in the signature.
   */
  protected static Class<?>[] getTypes(String signature, int numparams) {
    Class<?>[] types = new Class<?>[numparams];

    // From the sig, we can infer if these were basic types or not.
    int j = signature.indexOf("(") + 1, k = signature.indexOf(";", j);
    if (k < j) {
      k = signature.indexOf(")", j);
    }

    for (int i = 0; i < numparams; i++) {
      types[i] = getType(signature.substring(j, j + 1));

      // Determines whether to skip by one char, or until the next
      // semicolon.
      if (types[i] == void.class || simpleType(types[i])) {
        j++;
        k++;
      } else {
        j = k;
        k = signature.indexOf(";", k);
        if (k < j) {
          k = signature.indexOf(")", k);
        }
      }
    }
    return types;
  }

  /**
   * Gets the type class specified by the type signature.
   * 
   * @param p
   *          The type signature.
   * @return the type class specified by the type signature.
   */
  protected static Class<?> getType(String p) {
    if (p.equals("B")) {
      return byte.class;
    } else if (p.equals("C")) {
      return char.class;
    } else if (p.equals("D")) {
      return double.class;
    } else if (p.equals("F")) {
      return float.class;
    } else if (p.equals("I")) {
      return int.class;
    } else if (p.equals("J")) {
      return long.class;
    } else if (p.equals("S")) {
      return short.class;
    } else if (p.equals("V")) {
      return void.class;
    } else if (p.equals("Z")) {
      return boolean.class;
    } else {
      return Object.class;
    }
  }

  /**
   * Serializes the object or simple Java type.
   * 
   * @param obj
   *          The data to serialize.
   * @param type
   *          The type of data.
   * @return The serialized data.
   */
  protected static String serialized(Object obj, Class<?> type) {
    if (type == void.class) {
      return "void";
    } else if (simpleType(type)) {
      return obj.toString();
    } else {
      return serialize(obj);
    }
  }

  /**
   * Returns true iff <code>type</code> is a simple Java type (int, bool, etc., but not void or Object).
   * 
   * @param type
   *          Type of data.
   * @return true iff <code>type</code> is a simple Java type.
   */
  protected static boolean simpleType(Class<?> type) {
    return type == int.class || type == byte.class || type == short.class || type == long.class
        || type == boolean.class || type == float.class || type == double.class;
  }

  /**
   * Serializes the given object.
   * 
   * @param obj
   *          Object to serialize.
   * @return The serialized object.
   */
  public static String serialize(Object obj) {
    return xstream.toXML(obj);
  }

  /**
   * Deserializes the given object.
   * 
   * @param xml
   *          XML string to deserialize into an object.
   * @return The deserialized object.
   */
  public static Object deserialize(String xml) {
    return xstream.fromXML(xml);
  }

  /**
   * Gets a handle on the XStream object used for serialization. This object can be used to set custom
   * Converters, ignore fields, etc.
   * 
   * @return A handle on the XStream object.
   */
  public static XStream getXStream() {
    return xstream;
  }
}

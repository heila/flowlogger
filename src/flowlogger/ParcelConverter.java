package flowlogger;

import android.os.Parcel;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Provides a specialized means of marshalling Cursor objects for our app. It is necessary due to the default
 * one consistently causing stack overflows.
 * 
 * @author sean
 */
public class ParcelConverter implements Converter {

  public ParcelConverter() {
  }

  public boolean canConvert(Class clazz) {
    return Parcel.class.isAssignableFrom(clazz); // This hijacks all Cursor subclasses.
    // return clazz.equals(Parcel.class); // This is more specific.
  }

  public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
    @SuppressWarnings("unused")
    Parcel obj = (Parcel) value;
    obj.readValue(null);
//    writer.startNode("testNode");
//    writer.setValue("");
    // writer.startNode("mCloseGuard");
    // writer.setValue(null);
    // writer.startNode("mContentProvider");
    // writer.setValue(null);
    // writer.startNode("mCursor");
    // writer.setValue(null);
    writer.endNode();
  }

  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    Parcel obj = null;
    // reader.moveDown();
    // obj.setName(reader.getValue());
    // reader.moveUp();
    return obj;
  }
}
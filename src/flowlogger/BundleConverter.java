package flowlogger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.os.Bundle;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.ReflectionConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;

/**
 * Unparcels Bundle objects to serialize data that may otherwise be stored as native pointers.
 * 
 * @author S. E. A. Nel, Heila
 */
public class BundleConverter extends ReflectionConverter {

  public BundleConverter(Mapper mapper, ReflectionProvider reflectionProvider) {
    super(mapper, reflectionProvider);
  }

  @SuppressWarnings("rawtypes")
  public boolean canConvert(Class clazz) {
    return Bundle.class.isAssignableFrom(clazz); // This hijacks all Bundle
    // subclasses.
    // return clazz.equals(Bundle.class); // This is more specific.
  }

  public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
    // Converts native package data to a map from string to string.
    try {
      // obj.unparcel() is package private. Must use getDeclaredMethod and set accessible to true.
      Method m = Bundle.class.getDeclaredMethod("unparcel", (Class[]) null);
      m.setAccessible(true); // package private
      m.invoke(value, (Object[]) null);
    } catch (Exception e) {
      e.printStackTrace();
    }

    // Marshalls everything incl. classloader
    // super.marshal(value, writer, context);

    // Custom marshalling excl. classloader
    try {
      marshalField("mMap", value, writer, context);
      marshalField("mParcelledData", value, writer, context);
      marshalField("mHasFds", value, writer, context);
      marshalField("mFdsKnown", value, writer, context);
      marshalField("mAllowFds", value, writer, context);
      // If we don't include the classloader, add a new one on demarshalling manually to avoid an
      // Exception being thrown.
      // marshalField("mClassLoader", value, writer, context);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    // Unmarshalls everything the default way.
    // Bundle bundle = (Bundle) super.unmarshal(reader, context);

    // Unmarshalls fields the custom way, with added debugging.
    Bundle bundle = new Bundle();
    while (reader.hasMoreChildren()) {
      reader.moveDown();
      try {
        unmarshalField(reader.getNodeName(), bundle, reader, context);
      } catch (Exception e) {
        e.printStackTrace();
      }
      reader.moveUp();
    }

    // Sets the classloade
    try {
      Field mClassLoader = getPrivateField("mClassLoader", Bundle.class);
      mClassLoader.set(bundle, getClass().getClassLoader());
    } catch (Exception e) {
      e.printStackTrace();
    }

    return bundle;
  }

  protected void marshalField(String fieldName, Object instance, HierarchicalStreamWriter writer,
                              MarshallingContext context) throws NoSuchFieldException,
      IllegalAccessException, IllegalArgumentException {
    Field field = getPrivateField(fieldName, Bundle.class);
    Object obj = field.get(instance);
    // Do not set value to null (causes Exception), omit field instead.
    if (obj != null) {
      writer.startNode(fieldName);
      context.convertAnother(obj);
      writer.endNode();
    }
  }

  protected void unmarshalField(String fieldName, Object instance, HierarchicalStreamReader reader,
                                UnmarshallingContext context) throws NoSuchFieldException,
      IllegalAccessException, IllegalArgumentException {
    // System.out.println("unmarshallField: " + fieldName); // debug
    Field field = getPrivateField(fieldName, Bundle.class);
    Object val = context.convertAnother(instance, field.getType());
    // System.out.println("unmarshallField value: " + val); // debug
    field.set(instance, val);
  }

  protected Field getPrivateField(String fieldName, Class<?> clazz) throws NoSuchFieldException,
      IllegalAccessException, IllegalArgumentException {
    Field field = clazz.getDeclaredField(fieldName);
    field.setAccessible(true); // if private/package private
    return field;
  }
}
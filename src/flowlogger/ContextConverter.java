package flowlogger;

import android.content.Context;
import android.util.Log;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Context should not be marshaled/unmarshaled, it should be set to the actual context by the code when the
 * object storing this context has been unmarshaled.
 * 
 * @author Heila
 */
public class ContextConverter implements Converter {
  @SuppressWarnings("rawtypes")
  public boolean canConvert(Class object) {
    return Context.class.isAssignableFrom(object);
  }

  public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
    Log.i("Flowlogger", "marshaling Context");
    writer.startNode("empty");
    writer.endNode();
  }

  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    return null;
  }
}

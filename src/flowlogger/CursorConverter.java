package flowlogger;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Provides a specialized means of marshalling Cursor objects for our app. It is necessary due to the default
 * one consistently causing stack overflows.
 * 
 * TODO should be updated to work without content resolver since we might not have a context at this point to
 * receive the resolver from.
 * 
 * @author Sean, Heila
 */
public class CursorConverter implements Converter {
  protected ContentResolver mContentResolver;

  public CursorConverter() {
    // this.mContentResolver = mContentResolver;
  }

  // This attempts to get a handle on a specific cursor implementation class used by the app.
  // {
  // try {
  // clazz = Class.forName("android.content.ContentResolver$CursorWrapperInner");
  // } catch (ClassNotFoundException e) {
  // e.printStackTrace();
  // }
  // }

  public boolean canConvert(Class clazz) {
    return Cursor.class.isAssignableFrom(clazz); // This hijacks all Cursor subclasses.
    // return clazz.equals(Cursor.class); // This is more specific.
  }

  public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
    // For now we ignore all the cursor's fields.
    Cursor obj = (Cursor) value;
    writer.startNode("testNode");
    writer.setValue("");
    // writer.startNode("mCloseGuard");
    // writer.setValue(null);
    // writer.startNode("mContentProvider");
    // writer.setValue(null);
    // writer.startNode("mCursor");
    // writer.setValue(null);
    writer.endNode();
  }

  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    // This creates a brand new Cursor, the same way as done by the app. Nothing is actually deserialized.
    Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
    // Cursor obj = mContentResolver.query(uri, null, MediaStore.Audio.Media.IS_MUSIC + " = 1", null, null);
    // reader.moveDown();
    // obj.setName(reader.getValue());
    // reader.moveUp();
    return null;
  }
}